var React = require("-aek/react");
var Container = require("-components/container");
var {VBox,Panel} = require("-components/layout");
var {BannerHeader} = require("-components/header");
var {BasicSegment} = require("-components/segment");
var {Listview,Item} = require("-components/listview");
var xml2js = require("xml2js");
var request  = require('-aek/request');
var Input = require("@ombiel/aek-lib/react/components/input");
var Button = require("@ombiel/aek-lib/react/components/button");
// #####################################################################################

var Screen = React.createClass(
{
  getInitialState: function()
  {
    return {searchString: ''};
  },

  componentDidMount: function()
  {
   this.getData();
  },

  getData: function()
  {
    request.get("https://www.umt.edu/academics/degree-inventory/feed.xml")
    .end((e, res)=>
    {
      this.setState({data: res});
    });
  },

  handleChange: function(e)
  {
    this.setState({searchString: e.target.value});
  },

  render:function()
  {
    var parseString = xml2js.parseString;
    var degrees = null;
    if(this.state.data)
    {
      parseString(this.state.data.text, function (err, result)
      {
        degrees = result.academic_inventory.subject;
      });
    }
    var loading = !degrees;

    // Preps the search bar.
    var searchString = this.state.searchString.toLowerCase().trim();
    if (searchString.length > 0)
    {
      degrees = degrees.filter(
        function(subject)
        {
          return subject.name[0].toLowerCase().match(searchString);
        }
      );
    }

    return (
      <Container>
        <VBox>
          <Panel>
            <BannerHeader theme="prime" icon="university" key="header" flex={0}>
              Degrees &amp; Majors
            </BannerHeader>
            <BasicSegment loading={loading}>

            {/* Displays the search bar. */}
            <div style={{marginBottom:"1em"}}>
              <Input icon="search" name="name" fluid size="large?">
              <input type="text"
                placeholder="Search..."
                value={this.state.searchString}
                onChange={this.handleChange} />
              </Input>
            </div>

            <Listview items={degrees} itemFactory={(subject)=>
            {
              return (
                <Item>
                  {/* Displays the major. */}
                  <h2><a href={subject.department_url}> {subject.name} </a></h2>

                  <Listview items={subject.level} itemFactory={(level)=>
                  {
                    {/* Loads the options in an array. */}
                    var display = [];
                    if (subject.option)
                    {
                      for (var i = 0; i < subject.option.length; i++)
                      {
                        for (var j = 0; j < subject.option[i].level.length; j++)
                        {
                          if (subject.option[i].level[j] != 'undefined')
                          {
                            if (String(subject.option[i].level[j].name) == String(level.name))
                            {
                              display.push(subject.option[i].name);
                            }
                          }
                        }
                      }

                      {/* Displays the options if they exist. */}
                      if (display.length != 0)
                      {
                        return (
                          <Item active="true">
                            <p> {level.name} </p>
                            <Listview flush="true" borderless="false" items={display} itemFactory={(optionTitle)=>
                            {
                              return (
                                <Item active="true" className="optionItem">
                                  <ul>
                                    <li><p className="optionTitle"><b><em> {optionTitle} </em></b></p></li>
                                  </ul>
                                </Item>
                              );
                            }}/>
                          </Item>
                        );
                      }
                      else
                      {
                        return (
                          <Item active="true">
                            <p> {level.name} </p>
                          </Item>
                        );
                      }

                    }
                    else
                    {
                      return (
                        <Item active="true">
                          {/* Displays the available degrees for that major. */}
                          <p> {level.name} </p>
                        </Item>
                      );
                    }
                  }}/>
                </Item>
              );
            }}/>
            </BasicSegment>
          </Panel>
        </VBox>
      </Container>
    );
  }
});

React.render(<Screen/>,document.body);
